using System;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Warehouse.Areas.Attachment.Services;
using Warehouse.Areas.Compliance.Services;
using Warehouse.Areas.DocStore.Services;
using Warehouse.Areas.EmailCenter.Services;
using Warehouse.Controllers;
using Warehouse.Services;

namespace Warehouse.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            container.RegisterType<AccountController>(new InjectionConstructor(typeof(IClientService)));
            container.RegisterType<ManageController>(new InjectionConstructor());

            container.RegisterType<IClientService, ClientService>();
            container.RegisterType<IBranchService, BranchService>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IEmailService, Areas.EmailCenter.Services.EmailService>();

            container.RegisterType<IClientDocumentOwnershipService, ClientDocumentOwnershipService>();
            container.RegisterType<IDocumentService, DocumentService>();
            container.RegisterType<IDocumentProviderService, GeneralDocumentProviderAdminService>(
                "GeneralDocumentProviderAdminService",
                new InjectionConstructor());
            container.RegisterType<IDocumentProviderService, GeneralDocumentProviderClientService>(
                "GeneralDocumentProviderClientService",
                new InjectionConstructor());
            container.RegisterType<IDocumentProviderService, ClientDocumentProviderAdminService>(
                "ClientDocumentProviderAdminService",
                new InjectionConstructor());
            container.RegisterType<IDocumentProviderService, ClientDocumentProviderClientService>(
                "ClientDocumentProviderClientService",
                new InjectionConstructor());

            container.RegisterType<ICategoryService, CategoryService>();
            container.RegisterType<ICategoryProviderService, GeneralCategoryProviderAdminService>(
                "GeneralCategoryProviderAdminService",
                new InjectionConstructor());
            container.RegisterType<ICategoryProviderService, GeneralCategoryProviderClientService>(
                "GeneralCategoryProviderClientService",
                new InjectionConstructor());
            container.RegisterType<ICategoryProviderService, ClientCategoryProviderAdminService>(
                "ClientCategoryProviderAdminService",
                new InjectionConstructor());
            container.RegisterType<ICategoryProviderService, ClientCategoryProviderClientService>(
                "ClientCategoryProviderClientService",
                new InjectionConstructor());

            container.RegisterType<IAttachmentService, AttachmentService>();

            container.RegisterType<IClientStyleService, ClientStyleService>();

            container.RegisterType<ITaskService, TaskService>();
            container.RegisterType<IJobService, JobService>();
        }
    }
}
