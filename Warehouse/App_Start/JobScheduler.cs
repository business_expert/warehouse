﻿using System;
using System.Configuration;
using System.Globalization;
using Quartz;
using Quartz.Impl;
using Warehouse.Areas.Compliance.Jobs;

namespace Warehouse
{
    public class JobScheduler
    {
        public static void Start()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            var jobTime = ConfigurationManager.AppSettings["TaskNotificationJobTime"];
            var jobDateTime = DateTime.ParseExact(jobTime, "HH:mm", CultureInfo.InvariantCulture);

            IJobDetail job = JobBuilder.Create<TaskNotificationJob>().Build();
            ITrigger trigger = TriggerBuilder.Create()
                .WithDailyTimeIntervalSchedule
                  (s => s.WithIntervalInHours(24).
                          OnEveryDay().
                          StartingDailyAt(
                            TimeOfDay.HourAndMinuteOfDay(jobDateTime.Hour, jobDateTime.Minute)
                          ))
                .Build();

            //ITrigger trigger = TriggerBuilder.Create().StartNow()
            //                    .WithSimpleSchedule(x => x
            //                        .WithIntervalInSeconds(10))
            //                    //.RepeatForever())
            //                    .Build();

            scheduler.ScheduleJob(job, trigger);
        }
    }
}