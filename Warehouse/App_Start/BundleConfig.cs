﻿using System.Configuration;
using System.Web;
using System.Web.Optimization;

namespace Warehouse
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.UseCdn = true;

            bundles.Add(new ScriptBundle("~/bundles/jquery", ConfigurationManager.AppSettings["jqueryCdn"]).Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui", ConfigurationManager.AppSettings["jqueryuiCdn"]).Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular", ConfigurationManager.AppSettings["angularCdn"]).Include(
                        "~/Scripts/angular/angular.js"));

            bundles.Add(new ScriptBundle("~/bundles/datetimepicker").Include(
                        "~/Scripts/moment.js",
                        "~/Scripts/bootstrap-datetimepicker.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/bootstrap.css",
                        "~/Content/jquery-ui.css",
                        "~/Content/bootstrap-datetimepicker.css",
                        "~/Content/site.css",
                        "~/Areas/DocStore/Content/DocStore.css"));

            Areas.DocStore.BundleConfig.RegisterBundles(bundles, "~/Areas");
            Areas.Attachment.BundleConfig.RegisterBundles(bundles, "~/Areas");

            bundles.Add(new ScriptBundle("~/bundles/applications").Include(
                        "~/Scripts/applications.js"));

            bundles.Add(new ScriptBundle("~/bundles/Account").Include(
                        "~/Scripts/Controllers/RegisterUserController.js"));

            //BundleTable.EnableOptimizations = true;
        }
    }
}
