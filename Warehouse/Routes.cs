using System.Web.Mvc;
using System.Web.Routing;
using RestfulRouting;
using Warehouse.Controllers;

[assembly: WebActivator.PreApplicationStartMethod(typeof(Warehouse.Routes), "Start")]

namespace Warehouse
{
    public class Routes : RouteSet
    {
        public override void Map(IMapper map)
        {
            map.DebugRoute("routedebug");

            map.Root<HomeController>(x => x.Index());

            map.Resource<AccountController>(account =>
            {
                account.Route(
                    new Route("Account/{action}",
                        new RouteValueDictionary(new { controller = "Account" }),
                        new MvcRouteHandler()
                        )
                    );
            });

            map.Resource<ManageController>(extras =>
            {
                extras.Route(
                    new Route("Manage/{action}",
                        new RouteValueDictionary(new { controller = "Manage" }),
                        new MvcRouteHandler()
                        )
                    );
            });
            map.Resource<ClientStyleController>(clientStyle =>
            {
                clientStyle.Only("show");
            });
            map.Resources<BranchController>();
            map.Connect<ApiV1Routes>("api/v1", new[] { "Warehouse.Controllers.API.V1" });

            map.Connect<Areas.DocStore.Routes>();
            map.Connect<Areas.Attachment.Routes>();
            map.Connect<Areas.Setting.Routes>();
            map.Connect<Areas.Compliance.Routes>();
            map.Connect<Areas.AdviceLine.Routes>();
        }

        public static void Start()
        {
            var routes = RouteTable.Routes;
            routes.MapRoutes<Routes>();
        }
    }

    public class ApiV1Routes : RouteSet
    {
        public override void Map(IMapper map)
        {
            map.Resources<Controllers.API.V1.ApiClientsController>();
        }
    }
}