﻿var defaultApp = getDefaultApp();

defaultApp.controller("RegisterUserController", ["$scope", "$attrs", "$element", "$http", function ($scope, $attrs, $element, $http) {
    var clientsPath = $attrs.clientsPath;
    var branchesPath = $attrs.branchesPath;

    $scope.SelectedRole;

    $scope.Clients;
    $scope.SelectedClient;

    $scope.Branches;
    $scope.SelectedBranches;

    $scope.ShowClientSelection = function () {
        var role = $scope.SelectedRole;
        return role && role.search(/^Client::(User|Manager)$/) >= 0;
    };

    //Single selection of branch
    $scope.ShowBranchAsDropdown = function () {
        return $scope.SelectedRole === "Client::User" && $scope.SelectedClient;
    };

    //Multi selection of branches
    $scope.ShowBranchAsCheckboxes = function () {
        return $scope.SelectedRole === "Client::Manager" && $scope.SelectedClient;
    };

    $scope.OnRoleSelectionChange = function () {
        $scope.SelectedClient = null;
        $scope.SelectedBranches = null;

        if ($scope.ShowClientSelection()) {
            $http.get(clientsPath).
                success(function(response) {
                    var clients = response;
                    $scope.Clients = clients;
                });
        }
    };

    $scope.OnClientSelectionChange = function () {
        $scope.SelectedBranches = null;

        var selectedClient = $scope.SelectedClient && $scope.SelectedClient.Id;
        if (selectedClient) {
            $http.get(branchesPath + "?clientId=" + selectedClient).
                success(function(response) {
                    var branches = response;
                    $scope.Branches = branches;
                });
        }
    };
}]);