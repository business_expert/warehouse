﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Warehouse.Libraries.UsersManagement;
using Warehouse.Services;

namespace Warehouse.Controllers.API.V1
{
    public class ApiClientsController : Controller
    {
        private readonly IClientService _clientService;

        public ApiClientsController(IClientService clientService)
        {
            _clientService = clientService;
        }

        [Authorize(Roles = ApplicationRole.Admin)]
        public ActionResult Index()
        {
            var clients = _clientService.Clients();
            var clientsInfo = clients.Select(client => new { client.Id, client.Name });
            return Json(clientsInfo.ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}