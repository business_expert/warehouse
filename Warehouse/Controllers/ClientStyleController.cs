﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Warehouse.Services;

namespace Warehouse.Controllers
{
    public class ClientStyleController : Controller
    {
        private readonly IClientStyleService _clientStyleService;

        public ClientStyleController(IClientStyleService clientStyleService)
        {
            _clientStyleService = clientStyleService;
        }

        public ActionResult Show()
        {
            var styles = _clientStyleService.GetClientStyles();
            ViewBag.StyleDefinitions = styles;

            HttpContext.Response.ContentType = "text/css";
            return View();
        }
    }
}