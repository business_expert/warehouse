﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Warehouse.Libraries.UsersManagement;
using Warehouse.Services;

namespace Warehouse.Controllers
{
    public class BranchController : Controller
    {
        private readonly IClientService _clientService;

        public BranchController(IClientService clientService)
        {
            _clientService = clientService;
        }

        [Authorize(Roles = ApplicationRole.Admin)]
        public ActionResult Index(int clientId)
        {
            var branches = _clientService.BranchesForClient(clientId);
            var branchesInfo = branches.Select(branch => new { branch.Id, branch.Name });
            return Json(branchesInfo.ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}