namespace Warehouse.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdviceLines",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ConsultantId = c.String(maxLength: 128),
                        RaisedById = c.String(nullable: false, maxLength: 128),
                        Title = c.String(maxLength: 1000),
                        Description = c.String(),
                        PrivateNotes = c.String(),
                        Risk = c.String(),
                        RaisedMethod = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ConsultantId)
                .ForeignKey("dbo.AspNetUsers", t => t.RaisedById, cascadeDelete: true)
                .Index(t => t.ConsultantId)
                .Index(t => t.RaisedById);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Fullname = c.String(),
                        ClientId = c.Int(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.ClientId, cascadeDelete: true)
                .Index(t => t.ClientId)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.ApplicationUserBranches",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApplicationUserId = c.String(maxLength: 128),
                        BranchId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserId)
                .ForeignKey("dbo.Branches", t => t.BranchId, cascadeDelete: true)
                .Index(t => t.ApplicationUserId)
                .Index(t => t.BranchId);
            
            CreateTable(
                "dbo.Branches",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 300),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        City = c.String(),
                        County = c.String(),
                        PostCode = c.String(),
                        Telephone = c.String(),
                        Fax = c.String(),
                        ClientId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.ClientId, cascadeDelete: true)
                .Index(t => new { t.ClientId, t.Name }, unique: true, name: "BranchUnique");
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 300),
                        Address = c.String(),
                        IsAdminClient = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.ClientStyles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Key = c.String(),
                        Value = c.String(),
                        ClientId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.ClientId, cascadeDelete: true)
                .Index(t => t.ClientId);
            
            CreateTable(
                "dbo.Contracts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        ClientId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.ClientId, cascadeDelete: true)
                .Index(t => t.ClientId);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Attachments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Filesize = c.Int(nullable: false),
                        MimeType = c.String(nullable: false),
                        Path = c.String(),
                        Confirmed = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocumentType = c.String(nullable: false, maxLength: 200),
                        Name = c.String(nullable: false, maxLength: 200),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.DocumentType, t.Name }, unique: true, name: "CategoryUnique");
            
            CreateTable(
                "dbo.ContractCategoryRelations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ContractId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Contracts", t => t.ContractId, cascadeDelete: true)
                .Index(t => t.ContractId)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.Documents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocumentType = c.String(nullable: false),
                        Title = c.String(nullable: false),
                        Notes = c.String(),
                        Published = c.Boolean(nullable: false),
                        CategoryId = c.Int(),
                        SubcategoryId = c.Int(),
                        AttachmentId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Attachments", t => t.AttachmentId, cascadeDelete: true)
                .ForeignKey("dbo.Categories", t => t.CategoryId)
                .ForeignKey("dbo.Subcategories", t => t.SubcategoryId)
                .Index(t => t.CategoryId)
                .Index(t => t.SubcategoryId)
                .Index(t => t.AttachmentId);
            
            CreateTable(
                "dbo.Subcategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                        CategoryId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .Index(t => new { t.Name, t.CategoryId }, unique: true, name: "SubcategoryUnique");
            
            CreateTable(
                "dbo.ClientDocumentOwnerships",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OwnerType = c.String(nullable: false),
                        OwnerId = c.String(nullable: false),
                        ClientId = c.Int(nullable: false),
                        DocumentId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.ClientId, cascadeDelete: true)
                .ForeignKey("dbo.Documents", t => t.DocumentId, cascadeDelete: true)
                .Index(t => t.ClientId)
                .Index(t => t.DocumentId);
            
            CreateTable(
                "dbo.Jobs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaskId = c.Int(nullable: false),
                        RunAt = c.DateTime(nullable: false),
                        State = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tasks", t => t.TaskId, cascadeDelete: true)
                .Index(t => t.TaskId);
            
            CreateTable(
                "dbo.Tasks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ResponsibleUserId = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false),
                        Type = c.String(),
                        Description = c.String(),
                        Schedule = c.String(),
                        Completed = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ResponsibleUserId, cascadeDelete: true)
                .Index(t => t.ResponsibleUserId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Tasks", "ResponsibleUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Jobs", "TaskId", "dbo.Tasks");
            DropForeignKey("dbo.ClientDocumentOwnerships", "DocumentId", "dbo.Documents");
            DropForeignKey("dbo.ClientDocumentOwnerships", "ClientId", "dbo.Clients");
            DropForeignKey("dbo.Documents", "SubcategoryId", "dbo.Subcategories");
            DropForeignKey("dbo.Subcategories", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.Documents", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.Documents", "AttachmentId", "dbo.Attachments");
            DropForeignKey("dbo.ContractCategoryRelations", "ContractId", "dbo.Contracts");
            DropForeignKey("dbo.ContractCategoryRelations", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.AdviceLines", "RaisedById", "dbo.AspNetUsers");
            DropForeignKey("dbo.AdviceLines", "ConsultantId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Contracts", "ClientId", "dbo.Clients");
            DropForeignKey("dbo.ClientStyles", "ClientId", "dbo.Clients");
            DropForeignKey("dbo.Branches", "ClientId", "dbo.Clients");
            DropForeignKey("dbo.AspNetUsers", "ClientId", "dbo.Clients");
            DropForeignKey("dbo.ApplicationUserBranches", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.ApplicationUserBranches", "ApplicationUserId", "dbo.AspNetUsers");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Tasks", new[] { "ResponsibleUserId" });
            DropIndex("dbo.Jobs", new[] { "TaskId" });
            DropIndex("dbo.ClientDocumentOwnerships", new[] { "DocumentId" });
            DropIndex("dbo.ClientDocumentOwnerships", new[] { "ClientId" });
            DropIndex("dbo.Subcategories", "SubcategoryUnique");
            DropIndex("dbo.Documents", new[] { "AttachmentId" });
            DropIndex("dbo.Documents", new[] { "SubcategoryId" });
            DropIndex("dbo.Documents", new[] { "CategoryId" });
            DropIndex("dbo.ContractCategoryRelations", new[] { "CategoryId" });
            DropIndex("dbo.ContractCategoryRelations", new[] { "ContractId" });
            DropIndex("dbo.Categories", "CategoryUnique");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.Contracts", new[] { "ClientId" });
            DropIndex("dbo.ClientStyles", new[] { "ClientId" });
            DropIndex("dbo.Clients", new[] { "Name" });
            DropIndex("dbo.Branches", "BranchUnique");
            DropIndex("dbo.ApplicationUserBranches", new[] { "BranchId" });
            DropIndex("dbo.ApplicationUserBranches", new[] { "ApplicationUserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUsers", new[] { "ClientId" });
            DropIndex("dbo.AdviceLines", new[] { "RaisedById" });
            DropIndex("dbo.AdviceLines", new[] { "ConsultantId" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Tasks");
            DropTable("dbo.Jobs");
            DropTable("dbo.ClientDocumentOwnerships");
            DropTable("dbo.Subcategories");
            DropTable("dbo.Documents");
            DropTable("dbo.ContractCategoryRelations");
            DropTable("dbo.Categories");
            DropTable("dbo.Attachments");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.Contracts");
            DropTable("dbo.ClientStyles");
            DropTable("dbo.Clients");
            DropTable("dbo.Branches");
            DropTable("dbo.ApplicationUserBranches");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AdviceLines");
        }
    }
}
