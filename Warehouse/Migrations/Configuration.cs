using System.Globalization;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Warehouse.Areas.DocStore.Models;
using Warehouse.Libraries.UsersManagement;
using Warehouse.Models;

namespace Warehouse.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Warehouse.Models.ApplicationDbContext";
        }

        protected override void Seed(ApplicationDbContext context)
        {
            if (context.Users.Any()) { return; } //to avoid overwriting existing data, especially after live deployment

            AddRole(ApplicationRoleTypes.Client.ToString(), context);
            AddRole(ApplicationRoleTypes.Admin.ToString(), context);
            AddRole(ApplicationRolePositions.User.ToString(), context);
            AddRole(ApplicationRolePositions.Manager.ToString(), context);

            var warehouse = new Client { Name = "Warehouse", Address = "Warehouse address", IsAdminClient = true };
            var client = new Client { Name = "client user 1", Address = "client 1 address" };
            var client1 = new Client { Name = "client user 2", Address = "client 2 address" };
            context.Clients.AddOrUpdate(
                c => c.Name,
                warehouse,
                client,
                client1
                );
            context.SaveChanges();

            var warehouseUser = AddUser("user@warehouse.com", "warehouse user 1", warehouse, context, ApplicationRoleTypes.Admin.ToString(), ApplicationRolePositions.User.ToString());
            var user = AddUser("client_user1@test.com", "test user 1", client, context, ApplicationRoleTypes.Client.ToString(), ApplicationRolePositions.User.ToString());
            var user1 = AddUser("client_user2@test.com", "test user 2", client1, context, ApplicationRoleTypes.Client.ToString(), ApplicationRolePositions.User.ToString());

            context.ClientStyles.AddOrUpdate(
                s => new { s.ClientId, s.Key },
                new ClientStyle { Client = warehouse, Key = "backgroundTopColor", Value = "#CC0044" },
                new ClientStyle { Client = warehouse, Key = "backgroundBottomColor", Value = "#CC0044" },
                new ClientStyle { Client = warehouse, Key = "naviHeaderMobileButtonColor", Value = "white" },
                new ClientStyle { Client = warehouse, Key = "naviHeaderMobileButtonTextColor", Value = "black" },

                new ClientStyle { Client = client, Key = "backgroundTopColor", Value = "#1892AF" },
                new ClientStyle { Client = client, Key = "backgroundBottomColor", Value = "#1892AF" },
                new ClientStyle { Client = client, Key = "bodyBackgroundColor", Value = "#F1F1F1" },
                new ClientStyle { Client = client, Key = "naviHeaderTextColor", Value = "white" },
                new ClientStyle { Client = client, Key = "naviHeaderBrandColor", Value = "white" },
                new ClientStyle { Client = client, Key = "naviHeaderMobileButtonColor", Value = "white" },
                new ClientStyle { Client = client, Key = "naviHeaderMobileButtonTextColor", Value = "black" },

                new ClientStyle { Client = client1, Key = "logo", Value = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAAAyCAYAAAAZUZThAAAACXBIWXMAAAuJAAALiQE3ycutAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAC9BJREFUeNrsnX9wVcUVxz8bMzGTRmQYquBEGpUiZqiVpIpUixVbaVFK1VCtIv6orYOIVTs6ppRSmvHHlLZSdQZR+ssflWposahUfmoFBSraUqTRMpGhDEMppcggRia+7R97XnNzuT/2vvtu8kL2O3PnJe/du3fv7vmePefs2b1Ka42Dg0MwylwTODg4gjg4FAatdaoDuNO14hGlMLUcA/qC/MYdxRhBPtWLBOBRYGyK638JXBggVI8D4xOUcz9wQ8D38wLKz2OA3P8rlveokfOPtjj3XGA+8FvPd/OBJ4EpzsRKh8reom2AfkBFiuur5fAiB+wDLrAsY6gI74aQ8isi2vla4GrgBIv7VMj598acdxfwbWAvMBvoL/X4DrAamCCKpcwRJCGUUnlhqe0lz9tRhDJyAd+9CAwR4YrDxcABYFNI2bmY+zYCTQXW049bgAbg51LmZuA94H1gO7AAmC6Eme8Ikhw10mFD+nhQYrkI5BiLc88BXkl5v0Ei3GlQC3wOWAL8MeK8XcBMIf9NjiDJGzlPlN6AXEblfgjsACbGnPdZ+WyJqF9ZTN0HA4tEuM9IUecbgXbgMYtztwLrZeRzBElokwOc0sdHEIBnxccZGHHOOGAnsL8AEpd5zMSFwDbgNotychG+0IsJnu/HlmRyBPFgr2fI7y0jSFYk+ZN8XhryezlQB7ycsj8q5fNuoEoc66iygsqsk89XcciUIPs8nU8vIklW+CswMuS3z8vn4gL7JOf7fR/waxH2ixP2ba34jv9yFMiWIF5ToX8fMbFyMaPIQIKjepPELMrFlB1nYnl/fw7YAlwj5p1tPfOm8cGQ32rFr8wftXIMcASxhFLqWI/T1ohdbP5IHj3yBDnE4ZOGx4hwPVOE/vCfM0vuOatIz9AMvAv803O8K0ezI4g9jsfEzPMO+gmuzQD4O3C+77vLxLnekKL8KKf7fkyo/fqEI39VCEFOA06Vvj0FOAmYQR+cLEzzwP211h94OmwQDgBPy2e957tRwMaUo1xUX72OmVv5MjDM4h7vYELNg0MCL61yTpsc2+S7ckcQe1T6HHRHkE7hawe+IP/XiF+yLGMz8QER7pkR/kUebfKZJH+sfzeYqEcUQap8ZRzvuPF/vAZ8Wv6+UHyETZbCn6ZP7hbFdbMnaBJW3kbMrL4tRspzOIIkJEhfG0FstOgqTLLgeMyM9+oE/ZFL0WfbJRBwHvB2TH0fkbJsliucJf3b4QiS3MQK+79Un7ejG9qsVRzh5zFh08XdSNCnxWeIq+9eIdOZROdY1WMyexv7oolVkNOllPoYnZOE3g4YhEluK2Xtf4k4smUBWtv790Mp22wtJqq0A9hjeU0F8en4Nun638NEFa/E5FzdGkGmSkxK+2hM6slfxJQaKibYMGApJgOgwRHEDoM9ne5Nw55T4gT5G/BxTAgzjBj5dikPGG3aMLlUNnhK7rM0Qf22RrTfe8CPgP9alPMh8KCQMy41/jFgDXAFJtlyoi/g0Cz+0xj64ERhoctsxwBD5O86OpdpXur88yNCaR51JMtvdyy5rQH+HTAK1Tr56vXoAD5yzZDOSR8gk4R+p9GFeh0cQegasfKWUe2a1MERpKvz6h1BqlyTOhxpDlkiKKWOI3xGtQqTVrGnxJ5zCDAZuCfBNbcD6+i6qOhOUSpxodajMFvobPF9fzNmMu8PCeqwgq6z8Kdjkh8/ClBaHVK3soD6/ISuyxO+hAmw/NSyLmPk3v7w92Rp3yBZOoSJyrXRuaAsCF8DTrZs11ekTcLksxGTLHqc5+vtwEqt9QuZEwQTX98f8aDNJUiQ7ZhUieuBX1ic/xnMvMBC3/dnYvKsdliMvkFtOxIzy92G2UEkDqPkXC9BKjwmbj41JScC0U8E8oCvfjm5xttv9YQv7grCUHl+P0aLUmwLaJNjgBOBi4BvYrKO3wh5zpqQMoKUcBAxauhcZbleFNRekddzgOuUUpcD92qtW7MkSC1d5wL8s6uDLDu/u9EqHWFDkEukcYPmPJ4CXiiwDgeFpIeAqyzObw8YrV+Xw48RmDUh0y3r0k6yrIJDck2QDL0GzI24tgYzH9OE2YNrZ4CpvxEzz1MomoH9WutrfN9vBpaJ5dMMNCulpmutrebrCvFBThaNHDXClCKWYia6TrY4dxhmE4YgpNl4rtzT7ndlZC73REp63D13ANOEZFOLqKzzo8fZEiAKbVOt9W6t9Y3AcltyFEqQE+WBw8rIKmnxWEzy3y2YFIqkeFU0YNzWo/mtPZdl8AxVwNcxm7A1iB9QTDRS2qv+VpPNFlF1QoL3407UWj+StQ9SrbVuj/h9cJEeeiBm36ezMCvcKjFJePPoXM+QFP/AbBG6IOKc88QcyzJz9SXMFkDXYXKcPiiiArOdBe+J1YF7ImQuTSLkvqwqXEgjtceUUegIMhyTD/RdzKbL8zA7Fo4Ux7MJuCMFOcBk1ZaLwxmEYyQiE5WenpY4eQFpkraMsrtzBfSl7Sx4MTNzbctqIJvN514SU+vW7rYd/bbeQKI3PUtSZp1EUhokAlMhJkIeLZgQ5OOYDNNiYJMI+KUhgnmRfK6KGkFTKiSvMM3DpJJfCfymRLR8VgQZK0pwUcj1Bft2Wuu9SqlFwGVKqVMxEawNWuuD3UoQTP7VDIvGqfH5KRViKp0OfMLjKDcGXNtC555PazLozLWYaFYQxhG/dvyLQuoyghc4VWJeG2CzKds6qc8lmA0dtga0bamQJKweh8Ss7i/Kp8rTDtUyWo+SwMebHB46BxPdOw0zV0NEcOQt4OEQkixUSm3BhJOnAdOUUu1iVm8DVmmtd2RNEAKGyLIAR3G9CMjZmPeHVIeQwU+MA6Jhnsuwo1eIn1FP15j8UKlnS8z1+6XRy0IEuZzoeSB/e82VNmoCvpHCfOmpxUztovAeDQkatIiAzid8gq9cSPK2R+mUBbTb9piRZBMS5lZKnSukPAmz3mWCUmo3MEdrvS1Lguyy6Jg5CcprES30LJ07gmSJViHieB9Bxovwb425fmUKAocJcbO02e3Yz2z3hDkWVP8qadOZEiBQMpJMlt+XEL+nb6UonUeKVVGt9RqvBaKUqsfMwcxRSs3WWm/OqlF3FrHBWzCr2K7qJnLksZ7OTRXyGAX8OWNBDLt2myiI0Zi3PWXpKHvNmqToCFGy//EECPLnPCHK5gJLRZwpwbXWb8gk4l7C52JSV2quz7dIQ4zbRMss6AGz4HcexxHMLHQFdmvH03RklBA/IUSZmvLZbMO87QmfpYrwCF4YAZrkHnMoHSyla55WUTu7fxL7LYQYTSIEczFLQ3sCuyUQkN8BcZKYXT29XHiWELXZI8RJYRvm3ZGw/0+lsG1/HpSgzZQUyiMSSqnhCfs+M7u1Wik1toAHawHuw8xj3EdpJDOuFCeuHpNf9vtuum8uxuyZjwmH3pSx2ZFPgPyqxblHi5C/VcAzbcBMhk6g87ULRRuZlVL9MPlVN1heckYSMiat1PPAVKXUtZaN0yIjRbOMHNsoHbwkdd8oTuLyFHZ4MZXSCkw49HzMm3A7MurL3dIfk4h+6Q/AD+TzsQLv/ZDY/ncUewTRWu8XZTdOKXVFDJlqMBHMtbbllyeszK8ktnyZUuqTWusZBL/2oEXMlScxsf5SxC7PcNtKeLqHH3WYdR7lHB6SzHna9QCHhyVthfgezDxQ1s7rLBmx5sn9/NG5EZg5hYGkf4nnbOBnwPeBHwYoneMxofYygl/14JXZLT65fFgphcjlKOBxrfUbHmJUYkLOE4FdWusHMiGIVGahxJOnKqWeoeu8SIuYCU+STbJfsfEyJvt4bYJrGgheF+HHPsykVaGafo44xkn6qIXOvXltI1lXY9J7rpHDjz1SZmvKtt6JmeG+XMy6xb6AwXDiX1mdxxS/QhOSrJM2bxLC+LFEa53oNXJKtu4p1DGaiUmTyE/yLfFEiBx6H04Xf6xcBPrNHgyk2CrsMPkcIdGqduAdrfWegm9Q6CHaVwPfcrLl0FMEyfIoeARxcOgLKHNN4ODgCOLgUBD+NwDoojl2SaVTGgAAAABJRU5ErkJggg==" },
                new ClientStyle { Client = client1, Key = "backgroundTopColor", Value = "white" },
                new ClientStyle { Client = client1, Key = "backgroundBottomColor", Value = "white" },
                new ClientStyle { Client = client1, Key = "bodyBackgroundColor", Value = "#ECECEC" },
                new ClientStyle { Client = client1, Key = "naviHeaderTextColor", Value = "black" },
                new ClientStyle { Client = client1, Key = "naviHeaderBrandColor", Value = "black" },
                new ClientStyle { Client = client1, Key = "naviHeaderMobileButtonColor", Value = "black" },
                new ClientStyle { Client = client1, Key = "naviHeaderMobileButtonTextColor", Value = "white" }
                );

            var branch = new Branch { Name = "branch 1-1", Client = client };
            var branch0_1 = new Branch { Name = "branch 1-2", Client = client };
            var branch1 = new Branch { Name = "branch 2-1", Client = client1 };
            context.Branches.AddOrUpdate(
                b => b.Name,
                branch,
                branch0_1,
                branch1
                );
            context.SaveChanges();

            context.ApplicationUserBranches.AddOrUpdate(
                r => new { r.ApplicationUserId, r.BranchId },
                new ApplicationUserBranch { Branch = branch, ApplicationUser = user },
                new ApplicationUserBranch { Branch = branch1, ApplicationUser = user1 }
                );
            context.SaveChanges();

            var contract = new Contract
            {
                Client = client,
                StartDate = new DateTime(2016, 1, 1),
                EndDate = new DateTime(2020, 1, 1)
            };
            var contract1 = new Contract
            {
                Client = client1,
                StartDate = new DateTime(2016, 1, 1),
                EndDate = new DateTime(2020, 1, 1)
            };
            context.Contracts.AddOrUpdate(
                c => new { c.ClientId, c.StartDate, c.EndDate },
                contract,
                contract1
                );
            context.SaveChanges();

            var category1 = new Category { Name = "Test category 1", DocumentType = DocumentTypes.General.ToString() };
            var category2 = new Category { Name = "Test category 2", DocumentType = DocumentTypes.General.ToString() };
            var clientCategory1 = new Category { Name = "Client category 1", DocumentType = DocumentTypes.Client.ToString() };
            var clientCategory2 = new Category { Name = "Client category 2", DocumentType = DocumentTypes.Client.ToString() };
            context.Categories.AddOrUpdate(
                c => c.Name,
                category1,
                category2,
                clientCategory1,
                clientCategory2
                );
            context.SaveChanges();

            context.Subcategories.AddOrUpdate(
                s => s.Name,
                new Subcategory { Name = "Test subcategory 1-1", Category = category1 },
                new Subcategory { Name = "Test subcategory 1-2", Category = category1 },
                new Subcategory { Name = "Test subcategory 2-1", Category = category2 },
                new Subcategory { Name = "Client subcategory 1-1", Category = clientCategory1 },
                new Subcategory { Name = "Client subcategory 2-1", Category = clientCategory2 }
                );
            context.SaveChanges();

            context.ContractCategoryRelations.AddOrUpdate(
                r => new { r.ContractId, r.CategoryId },
                new ContractCategoryRelation { Category = category1, Contract = contract },
                new ContractCategoryRelation { Category = category2, Contract = contract },
                new ContractCategoryRelation { Category = category2, Contract = contract1 }
                );
            context.SaveChanges();
        }

        private ApplicationUser AddUser(string username, string fullname, Client client,
            ApplicationDbContext context, params string[] roles)
        {
            var user = context.Users.SingleOrDefault(u => u.UserName == username);
            if (user == null)
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                manager.UserValidator = new UserValidator<ApplicationUser>(manager) { AllowOnlyAlphanumericUserNames = false };
                user = new ApplicationUser
                {
                    Email = username,
                    UserName = username,
                    Fullname = fullname,
                    Client = client
                };

                manager.Create(user, "123456");
                context.SaveChanges();
                manager.AddToRoles(user.Id, roles);
                context.SaveChanges();
            }
            return user;
        }

        private void AddRole(string roleName, ApplicationDbContext context)
        {
            if (!context.Roles.Any(r => r.Name == roleName))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = roleName };

                manager.Create(role);
                context.SaveChanges();
            }
        }
    }
}
