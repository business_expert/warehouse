﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Warehouse.Models;
using AppContext = Warehouse.Libraries.AppContext;

namespace Warehouse.Services
{
    public interface IClientService
    {
        Client AddClient(Client client);
        Client UpdateClient(Client client);
        Client DeleteClient(Client client);
        List<Client> Clients();
        List<Branch> BranchesForClient(int clientId);
        Client GetClient(int clientId);
        Client GetClient(int clientId, ApplicationDbContext context);
        Client GetAdminClient();
        Client GetAdminClient(ApplicationDbContext context);
    }

    public class ClientService : IClientService
    {
        public Client AddClient(Client client)
        {
            var appContext = new AppContext();
            var saved = appContext.Context.Clients.Add(client);
            appContext.Context.SaveChanges();
            return saved;
        }

        public Client UpdateClient(Client client)
        {
            var appContext = new AppContext();
            var currentClient = appContext.Context.Clients.Find(client.Id);
            if (currentClient.Name != client.Name)
            {
                currentClient.Name = client.Name;
            }
            if (currentClient.Address != client.Address)
            {
                currentClient.Address = client.Address;
            }
            appContext.Context.SaveChanges();

            return currentClient;
        }

        public Client DeleteClient(Client client)
        {
            var appContext = new AppContext();
            var theClient = appContext.Context.Clients.Attach(client);
            appContext.Context.Clients.Remove(theClient);
            appContext.Context.SaveChanges();
            return theClient;
        }

        public List<Client> Clients()
        {
            var appContext = new AppContext();
            return appContext.Context.Clients.Where(c => !c.IsAdminClient).ToList();
        }

        public List<Branch> BranchesForClient(int clientId)
        {
            var appContext = new AppContext();
            var client = appContext.Context.Clients.Find(clientId);
            return client.Branches.ToList();
        }

        public Client GetAdminClient()
        {
            var appContext = new AppContext();
            return GetAdminClient(appContext.Context);
        }

        public Client GetAdminClient(ApplicationDbContext context)
        {
            var adminClient = context.Clients.First(c => c.IsAdminClient);
            return adminClient;
        }

        public Client GetClient(int clientId)
        {
            var appContext = new AppContext();
            return GetClient(clientId, appContext.Context);
        }

        public Client GetClient(int clientId, ApplicationDbContext context)
        {
            return context.Clients.Find(clientId);
        }
    }
}