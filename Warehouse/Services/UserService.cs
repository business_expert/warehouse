﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Warehouse.Libraries;
using Warehouse.Models;
using AppContext = Warehouse.Libraries.AppContext;

namespace Warehouse.Services
{
    public interface IUserService
    {
        List<ApplicationUser> Users();
        List<ApplicationUser> Users(int clientId);
    }

    public class UserService : IUserService
    {
        public List<ApplicationUser> Users()
        {
            var appContext = new AppContext();
            return appContext.Context.Users.ToList();
        }

        public List<ApplicationUser> Users(int clientId)
        {
            var appContext = new AppContext();
            var client = appContext.Context.Clients.Find(clientId);
            return client.ApplicationUsers.ToList();
        }
    }
}