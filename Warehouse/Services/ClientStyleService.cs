﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Warehouse.Models;
using AppContext = Warehouse.Libraries.AppContext;

namespace Warehouse.Services
{
    public interface IClientStyleService
    {
        Dictionary<string, string> GetClientStyles();
    }

    public class ClientStyleService : IClientStyleService
    {
        public Dictionary<string, string> GetClientStyles()
        {
            Dictionary<string, string> styles = new Dictionary<string, string>();
            var client = CurrentClient;

            if (client != null)
            {
                foreach (var style in client.ClientStyles)
                {
                    styles[style.Key] = style.Value;
                }
            }
            return styles;
        }

        private Client CurrentClient => new AppContext().CurrentUser?.Client;
    }
}