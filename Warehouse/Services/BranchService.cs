﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Warehouse.Libraries;
using Warehouse.Models;
using AppContext = Warehouse.Libraries.AppContext;

namespace Warehouse.Services
{
    public interface IBranchService
    {
        Branch AddBranch(int clientId, Branch branch);
        Branch GetBranch(int branchId);
        Branch UpdateBranch(Branch branch);
    }

    public class BranchService : IBranchService
    {
        public Branch AddBranch(int clientId, Branch branch)
        {
            var appContext = new AppContext();
            var client = appContext.Context.Clients.Find(clientId);
            client.Branches.Add(branch);
            appContext.Context.SaveChanges();
            return branch;
        }

        public Branch GetBranch(int branchId)
        {
            var appContext = new AppContext();
            var branch = appContext.Context.Branches.Find(branchId);
            return branch;
        }

        public Branch UpdateBranch(Branch branch)
        {
            var appContext = new AppContext();
            var currentBranch = appContext.Context.Branches.Find(branch.Id);
            EntityModelHelper.SetUpdatedValues(currentBranch, branch, "Name", "Address1", "Address2", "City", "County", "PostCode", "Telephone", "Fax");
            appContext.Context.SaveChanges();
            return currentBranch;
        }
    }
}