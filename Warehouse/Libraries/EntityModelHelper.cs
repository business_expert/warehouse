﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Warehouse.Areas.Compliance.Libs;

namespace Warehouse.Libraries
{
    public class EntityModelHelper
    {
        public static void SetUpdatedValues<T>(T current, T updated, params string[] properties)
        {
            foreach (var property in properties)
            {
                var prop = current.GetType().GetProperty(property);
                var currentValue = prop.GetValue(current);
                var updatedValue = prop.GetValue(updated);

                bool update;
                if (currentValue is Schedule) //TODO: need to find a more generic way
                {
                    update = !((Schedule) currentValue).Equals((Schedule) updatedValue);
                }
                else
                {
                    update = !currentValue.Equals(updatedValue);
                }

                if (update)
                {
                    prop.SetValue(current, updatedValue);
                }
            }
        }
    }
}