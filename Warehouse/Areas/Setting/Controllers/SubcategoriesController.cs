﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Warehouse.Areas.DocStore.Models;
using Warehouse.Areas.DocStore.Services;
using Warehouse.Libraries.UsersManagement;

namespace Warehouse.Areas.Setting.Controllers
{
    [Authorize(Roles = ApplicationRole.Admin)]
    public class SubcategoriesController : Controller
    {
        private readonly ICategoryService _categoryService;

        public SubcategoriesController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        public ActionResult Show()
        {
            return RedirectToAction("Edit");
        }

        public ActionResult New(int categoryId)
        {
            var category = _categoryService.GetCategory(categoryId);
            SetCategoryInfo(category.Id, category.Name);
            return View();
        }

        public ActionResult Create(int categoryId, Subcategory subcategory)
        {
            _categoryService.AddSubcategory(categoryId, subcategory);
            return RedirectToAction("Show", "Categories", new { area = "Setting", id = categoryId });
        }

        public ActionResult Edit(int id)
        {
            var subcategory = _categoryService.GetSubcategory(id);
            SetCategoryInfo(subcategory.CategoryId, subcategory.Category.Name);
            return View("New", subcategory);
        }

        public ActionResult Update(Subcategory subcategory)
        {
            _categoryService.UpdateSubcategory(subcategory);
            return RedirectToAction("Show");
        }

        private void SetCategoryInfo(int categoryId, string categoryName)
        {
            ViewBag.categoryId = categoryId;
            ViewBag.categoryName = categoryName;
        }
    }
}