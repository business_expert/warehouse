﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Warehouse.Libraries.UsersManagement;
using Warehouse.Models;
using Warehouse.Services;

namespace Warehouse.Areas.Setting.Controllers
{
    [Authorize(Roles = ApplicationRole.Admin)]
    public class ClientsController : Controller
    {
        private readonly IClientService _clientService;

        public ClientsController(IClientService clientService)
        {
            _clientService = clientService;
        }
        
        public ActionResult Index(int? page)
        {
            var clients = _clientService.Clients();
            var pageNumber = page ?? 1;
            var pagedClients = clients.ToPagedList(pageNumber, PageSize);
            return View(pagedClients);
        }

        public ActionResult New()
        {
            return View();
        }

        public ActionResult Create(Client client)
        {
            client.IsAdminClient = false; //there is only admin client across the app
            _clientService.AddClient(client);
            return RedirectToAction("Index");
        }

        public ActionResult Show(int id)
        {
            return RedirectToAction("Edit");
        }

        public ActionResult Edit(int id)
        {
            var client = _clientService.GetClient(id);
            return View("New", client);
        }

        public ActionResult Update(Client client)
        {
            _clientService.UpdateClient(client);
            return RedirectToAction("Edit");
        }

        public ActionResult Destroy(Client client)
        {
            _clientService.DeleteClient(client);
            return RedirectToAction("Index");
        }

        private int PageSize => Convert.ToInt32(ConfigurationManager.AppSettings["ListingPageSize"]);
    }
}