﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Warehouse.Areas.DocStore.Services;
using Warehouse.Libraries.UsersManagement;
using PagedList;
using Warehouse.Areas.DocStore.Models;

namespace Warehouse.Areas.Setting.Controllers
{
    [Authorize(Roles = ApplicationRole.Admin)]
    public class CategoriesController : Controller
    {
        private readonly ICategoryService _categoryService;

        public CategoriesController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        public ActionResult Index(int? page)
        {
            var categories = _categoryService.AllCategories();
            categories = categories.OrderBy(category => category.DocumentType).ToList();
            var pageNumber = page ?? 1;
            var pagedClients = categories.ToPagedList(pageNumber, PageSize);
            return View(pagedClients);
        }

        public ActionResult New()
        {
            SetDocumentTypes();
            return View();
        }

        public ActionResult Create(Category category)
        {
            _categoryService.AddCategory(category);
            return RedirectToAction("Index");
        }

        public ActionResult Show(int id)
        {
            return RedirectToAction("Edit");
        }

        public ActionResult Edit(int id)
        {
            SetDocumentTypes();
            var category = _categoryService.GetCategory(id);
            return View("New", category);
        }

        public ActionResult Update(Category category)
        {
            _categoryService.UpdateCategory(category);
            return RedirectToAction("Show");
        }

        private void SetDocumentTypes()
        {
            ViewBag.documentTypes = new SelectList(Enum.GetValues(typeof(DocumentTypes)).Cast<DocumentTypes>().ToList());
        }

        private int PageSize => Convert.ToInt32(ConfigurationManager.AppSettings["ListingPageSize"]);
    }
}