﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Warehouse.Libraries.UsersManagement;
using Warehouse.Models;
using Warehouse.Services;

namespace Warehouse.Areas.Setting.Controllers
{
    [Authorize(Roles = ApplicationRole.Admin)]
    public class BranchesController : Controller
    {
        private readonly IBranchService _branchService;
        private readonly IClientService _clientService;

        public BranchesController(IBranchService branchService, IClientService clientService)
        {
            _branchService = branchService;
            _clientService = clientService;
        }

        public ActionResult Show()
        {
            return RedirectToAction("Edit");
        }

        public ActionResult New(int clientId)
        {
            var client = _clientService.GetClient(clientId);
            SetClientInfo(clientId, client.Name);
            return View();
        }

        public ActionResult Create(int clientId, Branch branch)
        {
            _branchService.AddBranch(clientId, branch);
            return RedirectToAction("Show", "Clients", new { area = "Setting", id = clientId });
        }

        public ActionResult Edit(int id)
        {
            var branch = _branchService.GetBranch(id);
            SetClientInfo(branch.ClientId, branch.Client.Name);
            return View("New", branch);
        }

        public ActionResult Update(Branch branch)
        {
            _branchService.UpdateBranch(branch);
            return RedirectToAction("Show");
        }

        private void SetClientInfo(int clientId, string clientName)
        {
            ViewBag.clientId = clientId;
            ViewBag.clientName = clientName;
        }
    }
}