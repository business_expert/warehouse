﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Warehouse.Libraries.UsersManagement;

namespace Warehouse.Areas.Setting.Controllers
{
    [Authorize(Roles = ApplicationRole.Admin)]
    public class SettingController : Controller
    {
        public ActionResult Show()
        {
            return View();
        }
    }
}