﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using CronExpressionDescriptor;
using NCrontab;
using Newtonsoft.Json;

namespace Warehouse.Areas.Compliance.Libs
{
    public enum ScheduleTypes
    {
        Once,
        Repeative,
        Empty
    }

    public class Schedule : IEquatable<Schedule>
    {
        private const string DatetimeFormat = "s";
        private const string DateDisplayFormat = "yyyy/MM/dd";
        private const string DatetimeDisplayFormat = "yyyy/MM/dd HH:mm";

        public Schedule()
        {
            _data = new Dictionary<string, dynamic>();
        }

        public bool Equals(Schedule other)
        {
            if (Type != other.Type)
            {
                return false;
            }

            bool equal;
            switch (Type)
            {
                case ScheduleTypes.Empty:
                    equal = true;
                    break;
                case ScheduleTypes.Once:
                    equal = ConvertToDateTime(_data["runAt"]).CompareTo(ConvertToDateTime(other._data["runAt"])) == 0;
                    break;
                case ScheduleTypes.Repeative:
                    equal = ConvertToDateTime(_data["start"]).CompareTo(ConvertToDateTime(other._data["start"])) == 0 &&
                            ConvertToDateTime(_data["end"]).CompareTo(ConvertToDateTime(other._data["end"])) == 0 &&
                            _data["cron"] == other._data["cron"];
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return equal;
        }

        private Dictionary<string, dynamic> _data;

        public ScheduleTypes Type => (ScheduleTypes)Enum.Parse(typeof(ScheduleTypes), _data["type"]);

        /// <summary>
        /// Get the next occurrence from current time.
        /// </summary>
        /// <returns></returns>
        public DateTime? GetNextOccurrence()
        {
            DateTime start = DateTime.Now;
            DateTime? nextOccurrence;
            switch (Type)
            {
                case ScheduleTypes.Once:
                    nextOccurrence = ConvertToDateTime(_data["runAt"]);
                    if (nextOccurrence <= start)
                    {
                        nextOccurrence = null;
                    }
                    break;
                case ScheduleTypes.Repeative:
                    var cronSchedule = CrontabSchedule.Parse(_data["cron"]);
                    var originalStart = ConvertToDateTime(_data["start"]);
                    if (originalStart > start)
                    {
                        start = originalStart;
                    }
                    IEnumerable<DateTime> all = cronSchedule.GetNextOccurrences(start, ConvertToDateTime(_data["end"]));
                    if (all.Any())
                    {
                        nextOccurrence = all.First();
                    }
                    else
                    {
                        nextOccurrence = null;
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return nextOccurrence;
        }

        public Dictionary<string, dynamic> GetValue()
        {
            Dictionary<string, dynamic> data = new Dictionary<string, dynamic>();
            switch (Type)
            {
                case ScheduleTypes.Once:
                    data["runAt"] = ConvertToDateTime(_data["runAt"]).ToString(DatetimeDisplayFormat);
                    break;
                case ScheduleTypes.Repeative:
                    data["start"] = ConvertToDateTime(_data["start"]).ToString(DateDisplayFormat);
                    data["end"] = ConvertToDateTime(_data["end"]).ToString(DateDisplayFormat);
                    data["cron"] = _data["cron"];
                    break;
                case ScheduleTypes.Empty:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return data;
        }

        public override string ToString()
        {
            string str;
            switch (Type)
            {
                case ScheduleTypes.Once:
                    str = $"At {ConvertToDateTime(_data["runAt"]).ToString(DatetimeDisplayFormat)}";
                    break;
                case ScheduleTypes.Repeative:
                    var cronDescription = ExpressionDescriptor.GetDescription(_data["cron"]);
                    str = $"{cronDescription} between {ConvertToDateTime(_data["start"]).ToString(DateDisplayFormat)} and {ConvertToDateTime(_data["end"]).ToString(DateDisplayFormat)}";
                    break;
                case ScheduleTypes.Empty:
                    str = "";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return str;
        }

        public string Serialized
        {
            get { return JsonConvert.SerializeObject(_data); }
            set
            {
                if (string.IsNullOrEmpty(value)) return;

                var metaData = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(value);

                _data = metaData ?? new Dictionary<string, dynamic>();
            }
        }

        public void CreateOnceSchedule(DateTime runAt)
        {
            _data["type"] = ScheduleTypes.Once.ToString();
            _data["runAt"] = runAt.ToString(DatetimeFormat);
        }

        public void CreateRepeativeSchedule(DateTime start, DateTime end, string cron)
        {
            _data["type"] = ScheduleTypes.Repeative.ToString();
            _data["start"] = start.ToString(DatetimeFormat);
            _data["end"] = end.ToString(DatetimeFormat);
            _data["cron"] = cron;
        }

        public void CreateEmptySchedule()
        {
            _data["type"] = ScheduleTypes.Empty.ToString();
        }

        private DateTime ConvertToDateTime(dynamic value)
        {
            if (value is DateTime)
            {
                return value;
            }

            return DateTime.ParseExact(value, DatetimeFormat, CultureInfo.InvariantCulture);
        }
    }
}