﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Warehouse.Areas.Compliance.Libs;
using Warehouse.Areas.Compliance.Models;

namespace Warehouse.Areas.Compliance.Models
{
    //public class IdentityModels
    //{
    //}
}

namespace Warehouse.Models
{
    public partial class ApplicationDbContext
    {
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Job> Jobs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.ComplexType<Schedule>()
                        .Property(p => p.Serialized)
                        .HasColumnName("Schedule");
            modelBuilder.ComplexType<Schedule>().Ignore(s => s.Type);

            base.OnModelCreating(modelBuilder);
        }
    }
}