﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Warehouse.Models;

namespace Warehouse.Areas.Compliance.Models
{
    public class Job : EntityBase
    {
        [Required]
        public int TaskId { get; set; }
        public virtual Task Task { get; set; }

        [Required]
        public DateTime RunAt { get; set; }

        public string State { get; set; }
    }
}