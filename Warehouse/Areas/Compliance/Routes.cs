﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestfulRouting;
using Warehouse.Areas.Compliance.Controllers;

namespace Warehouse.Areas.Compliance
{
    public class Routes : RouteSet
    {
        public override void Map(IMapper map)
        {
            map.Area<TasksController>("Compliance", "", area =>
            {
                area.Resources<TasksController>();
            });

            //map.Area<SettingController>("Setting", "", area =>
            //{
            //    area.Resource<SettingController>(setting =>
            //    {
            //        setting.Only("show");
            //        setting.Resources<ClientsController>(client =>
            //        {
            //            client.Resources<BranchesController>();
            //        });
            //        setting.Resources<CategoriesController>(category =>
            //        {
            //            category.Resources<SubcategoriesController>();
            //        });
            //    });
            //});
        }
    }
}