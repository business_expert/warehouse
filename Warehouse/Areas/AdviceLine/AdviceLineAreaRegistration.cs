﻿using System.Web.Mvc;

namespace Warehouse.Areas.AdviceLine
{
    public class AdviceLineAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "AdviceLine";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            //context.MapRoute(
            //    "AdviceLine_default",
            //    "AdviceLine/{controller}/{action}/{id}",
            //    new { action = "Index", id = UrlParameter.Optional }
            //);
        }
    }
}