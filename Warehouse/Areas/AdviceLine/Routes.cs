﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestfulRouting;
using Warehouse.Areas.AdviceLine.Controllers;

namespace Warehouse.Areas.AdviceLine
{
    public class Routes : RouteSet
    {
        public override void Map(IMapper map)
        {
            map.Area<AdviceLinesController>("AdviceLine", "", area =>
            {
                area.Resources<AdviceLinesController>();
            });
        }
    }
}