﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using PagedList;
using Warehouse.Areas.Attachment.Services;
using Warehouse.Areas.DocStore.Models;
using Warehouse.Areas.DocStore.Services;
using Warehouse.Libraries.UsersManagement;
using Warehouse.Models;
using Warehouse.Services;
using AppContext = Warehouse.Libraries.AppContext;

namespace Warehouse.Areas.DocStore.Controllers
{
    [Authorize(Roles = ApplicationRole.Admin + "," + ApplicationRole.Client)]
    public class DocumentController : Controller
    {
        private readonly IDocumentService _documentService;
        private readonly ICategoryService _categoryService;
        private readonly IAttachmentService _attachmentService;
        private readonly IClientService _clientService;
        private readonly IClientDocumentOwnershipService _ownershipService;
        private readonly AppContext appContext;

        public DocumentController(AppContext appContext,
            IDocumentService documentService,
            ICategoryService categoryService,
            IAttachmentService attachmentService,
            IClientService clientService,
            IClientDocumentOwnershipService ownershipService)
        {
            _documentService = documentService;
            _categoryService = categoryService;
            _attachmentService = attachmentService;
            _clientService = clientService;
            _ownershipService = ownershipService;
            this.appContext = appContext;
        }

        private DocumentTypes DocumentType(string documentTypeId)
        {
            var documentType = (DocumentTypes)Enum.Parse(typeof(DocumentTypes), documentTypeId, true);
            return documentType;
        }

        public ActionResult Index(string documentTypeId, 
            string fileinfo, string filetype, 
            string category, string subcategory, 
            string client, string branch,
            string published, int? page)
        {
            Dictionary<string, string> searchCriteria = new Dictionary<string, string>();
            if (!string.IsNullOrWhiteSpace(fileinfo))
            {
                searchCriteria["fileinfo"] = fileinfo;
            }
            if (!string.IsNullOrWhiteSpace(filetype))
            {
                searchCriteria["filetype"] = filetype;
            }
            if (!string.IsNullOrWhiteSpace(published))
            {
                searchCriteria["published"] = published;
            }
            if (!string.IsNullOrWhiteSpace(category))
            {
                searchCriteria["category"] = category;
            }
            else //if category is not selected, deselect subcategory
            {
                subcategory = string.Empty;
            }
            if (!string.IsNullOrWhiteSpace(subcategory))
            {
                searchCriteria["subcategory"] = subcategory;
            }

            if (!appContext.IsCurrentUserAdmin)
            {
                client = appContext.CurrentUser.Client.Id.ToString();
            }
            if (!string.IsNullOrWhiteSpace(client))
            {
                searchCriteria["client"] = client;
            }
            else
            {
                branch = string.Empty;
            }
            if (!string.IsNullOrWhiteSpace(branch))
            {
                searchCriteria["branch"] = branch;
            }

            var documentType = DocumentType(documentTypeId);
            var documents = _documentService.Documents(documentType, searchCriteria);

            var pageNumber = page ?? 1;
            var pagedDocuments = documents.ToPagedList(pageNumber, PageSize);

            ViewBag.CurrentFilters = new Dictionary<string, string>()
            {
                { "fileinfo", fileinfo },
                { "filetype", filetype },
                { "published", published },
                { "category", category },
                { "subcategory", subcategory },
                { "client", client },
                { "branch", branch }
            };
            ViewBag.AvailableFiletypes = new SelectList(_documentService.DocumentFileFormats(documentType), filetype);
            ViewBag.PublishedStatuses = new SelectList(_documentService.DocumentPublishStatuses(), "Value", "Name", published);
            ViewBag.AvailableCategories = new SelectList(_categoryService.AllCategories(documentType), "Id", "Name", category);
            ViewBag.AvailableSubcategories = _categoryService.AllSubcategories(documentType);
            ViewBag.AvailableClients = new SelectList(_clientService.Clients(), "Id", "Name", client);
            if (!string.IsNullOrWhiteSpace(client))
            {
                ViewBag.AvailableBranches = new SelectList(_clientService.BranchesForClient(Convert.ToInt32(client)), "Id", "Name", branch);
            }
            else
            {
                ViewBag.AvailableBranches = new SelectList(Enumerable.Empty<Branch>());
            }
            
            var hiddenControls = appContext.IsCurrentUserAdmin ? new List<string>() : new List<string>() { "CreateDocument", "EditDocument", "PublishedStatus" };
            if (!(documentType == DocumentTypes.Client && appContext.IsCurrentUserAdmin))
            {
                hiddenControls.Add("SearchClient");
            }
            if (!(documentType == DocumentTypes.Client && (appContext.IsCurrentUserAdmin || appContext.IsCurrentUserClientManager)))
            {
                hiddenControls.Add("SearchBranch");
            }
            ViewBag.HiddenControls = hiddenControls;

            ViewBag.Title = $"{documentType} Documents";
            return View(pagedDocuments);
        }

        [Authorize(Roles = ApplicationRole.Admin)]
        public ActionResult New(string documentTypeId)
        {
            var documentType = DocumentType(documentTypeId);
            PopulateCategories(documentType);
            if (documentType == DocumentTypes.Client)
            {
                PopulateClients();
            }
            return View();
        }

        [Authorize(Roles = ApplicationRole.Admin)]
        public ActionResult Create(string documentTypeId, int? ownerClientId, int? ownerBranchId,
            [Bind(Include = "Title, Notes, Published, CategoryId, SubcategoryId, AttachmentId")]Document document)
        {
            var documentType = DocumentType(documentTypeId);

            ClientDocumentOwnership documentOwnership = null;
            if (documentType == DocumentTypes.Client)
            {
                BuildClientDocumentOwnership(ownerClientId, ownerBranchId, out documentOwnership);
            }
            _documentService.CreateDocument(documentType, document, documentOwnership);
            _attachmentService.ConfirmAttachment(document.AttachmentId);
            return RedirectToAction("Index");
        }

        [Authorize(Roles = ApplicationRole.Admin)]
        public ActionResult Edit(string documentTypeId, int id)
        {
            var documentType = DocumentType(documentTypeId);
            var document = _documentService.GetDocument(documentType, id);
            PopulateCategories(documentType, document.CategoryId);
            if (DocumentTypes.Client == documentType)
            {
                var ownership = _ownershipService.OwnershipForDocument(document.Id);
                if ("Branch" == ownership.OwnerType)
                {
                    PopulateClients(ownership.ClientId, Convert.ToInt32(ownership.OwnerId));
                }
                else
                {
                    PopulateClients(ownership.ClientId);
                }
            }
            return View("New", document);
        }

        [Authorize(Roles = ApplicationRole.Admin)]
        public ActionResult Update(string documentTypeId, int? ownerClientId, int? ownerBranchId, Document document)
        {
            var documentType = DocumentType(documentTypeId);
            var updatedFields = UpdatedFieldsFor(_documentFields);
            if (updatedFields.ContainsKey("AttachmentId"))
            {
                _attachmentService.ConfirmAttachment(Convert.ToInt32(updatedFields["AttachmentId"]));
            }
            ClientDocumentOwnership documentOwnership = null;
            if (documentType == DocumentTypes.Client)
            {
                BuildClientDocumentOwnership(ownerClientId, ownerBranchId, out documentOwnership);
            }
            _documentService.UpdateDocument(documentType, document, updatedFields, documentOwnership);

            var deletedAttachments = Request["DeletedAttachmentIds"];
            if (deletedAttachments != null)
            {
                var deletedAttachmentids = JsonConvert.DeserializeObject<int[]>(deletedAttachments);
                if (deletedAttachmentids != null)
                {
                    foreach (var attachmentId in deletedAttachmentids)
                    {
                        _attachmentService.DestroyAttachment(attachmentId);
                    }
                }
            }

            return Redirect(Request.UrlReferrer.PathAndQuery);
        }

        [Authorize(Roles = ApplicationRole.Admin)]
        public ActionResult Destroy(string documentTypeId, int id)
        {
            Document destroyedDocument;
            bool success = _documentService.Destroy(DocumentType(documentTypeId), id, out destroyedDocument);
            if (success)
            {
                _attachmentService.DestroyAttachment(destroyedDocument.AttachmentId);
                return RedirectToAction("Index");
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        private void PopulateCategories(DocumentTypes documentType, int? selectedCategoryId = null)
        {
            var categories = _categoryService.AllCategories(documentType);
            ViewBag.CategoryId = new SelectList(categories, "Id", "Name");

            var subcategories = _categoryService.AllSubcategories(documentType);
            ViewBag.AllSubcategories = subcategories;
            if (selectedCategoryId.HasValue)
            {
                var filteredSubcategories = subcategories.Where(sc => sc.CategoryId == selectedCategoryId).ToList();
                ViewBag.SubCategoryId = new SelectList(filteredSubcategories, "Id", "Name");
            }
            else
            {
                ViewBag.SubCategoryId = Enumerable.Empty<SelectListItem>();
            }
        }

        private void PopulateClients(int? selectedClient = null, int? selectedBranch = null)
        {
            var clients = _clientService.Clients();

            if (selectedClient.HasValue)
            {
                ViewBag.ClientList = new SelectList(clients, "Id", "Name", selectedClient.Value);
                if (selectedBranch.HasValue)
                {
                    ViewBag.OwnerBranchId = selectedBranch.Value;
                }
            }
            else
            {
                ViewBag.ClientList = new SelectList(clients, "Id", "Name");
            }
        }

        private void BuildClientDocumentOwnership(int? ownerClientId, int? ownerBranchId, 
            out ClientDocumentOwnership documentOwnership)
        {
            if (ownerClientId.HasValue)
            {
                documentOwnership = new ClientDocumentOwnership();
                if (ownerBranchId.HasValue)
                {
                    documentOwnership.OwnerType = "Branch";
                    documentOwnership.OwnerId = ownerBranchId.ToString();
                }
                else
                {
                    documentOwnership.OwnerType = "Client";
                    documentOwnership.OwnerId = ownerClientId.ToString();
                }

                documentOwnership.ClientId = ownerClientId.Value;
            }
            else
            {
                documentOwnership = null;
            }
        }

        private readonly string[] _documentFields = { "Title", "Notes", "Published", "CategoryId", "SubcategoryId", "AttachmentId" };
        private Dictionary<string, string> UpdatedFieldsFor(string[] fields)
        {
            var updatedFields = new Dictionary<string, string>();
            foreach (var field in fields)
            {
                var value = Request[field];
                if (value != null) //the value is null if not posted back
                {
                    updatedFields[field] = value;
                }
            }

            return updatedFields;
        }

        private int PageSize => Convert.ToInt32(ConfigurationManager.AppSettings["ListingPageSize"]);
    }
}
