﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Warehouse.Areas.DocStore.Libs;
using Warehouse.Models;

namespace Warehouse.Areas.DocStore.Models
{
    public class Document : EntityBase
    {
        [Required]
        [AllowedValues(AllowedValues = "General,Client")]
        public string DocumentType { get; set; }

        [Required]
        public string Title { get; set; }
        public string Notes { get; set; }
        public bool Published { get; set; }
        
        public int? CategoryId { get; set; }
        public virtual Category Category { get; set; }

        public int? SubcategoryId { get; set; }
        public virtual Subcategory Subcategory { get; set; }
        
        [Required(ErrorMessage = "Attachment is required")]
        public int AttachmentId { get; set; }
        public virtual Attachment.Models.Attachment Attachment { get; set; }
    }
}