﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Warehouse.Areas.DocStore.Libs;
using Warehouse.Models;

namespace Warehouse.Areas.DocStore.Models
{
    public class Category : EntityBase
    {
        [Required]
        [AllowedValues(AllowedValues = "General,Client")]
        [StringLength(200)]
        [Index("CategoryUnique", 1, IsUnique = true)]
        public string DocumentType { get; set; }

        [Required]
        [StringLength(200)]
        [Index("CategoryUnique", 2, IsUnique = true)]
        public string Name { get; set; }

        public virtual ICollection<Document> Documents { get; set; }
        public virtual ICollection<Subcategory> Subcategories { get; set; }
        public virtual ICollection<ContractCategoryRelation> ContractCategoryRelations { get; set; }
    }
}