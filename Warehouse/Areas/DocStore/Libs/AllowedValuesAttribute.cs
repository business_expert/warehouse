﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Warehouse.Areas.DocStore.Libs
{
    public class AllowedValuesAttribute:ValidationAttribute
    {
        public string AllowedValues;

        public override bool IsValid(object value)
        {
            var strValue = value as string;
            var values = AllowedValues.Split(',');
            return values.Contains(strValue);
        }
    }
}