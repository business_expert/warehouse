﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Warehouse.Areas.DocStore.Models;
using Warehouse.Libraries;
using AppContext = Warehouse.Libraries.AppContext;

namespace Warehouse.Areas.DocStore.Services
{
    public class ClientDocumentProviderAdminService : IDocumentProviderService
    {
        private readonly string _clientDocumentType = DocumentTypes.Client.ToString();
        
        public IQueryable<Document> UserDocuments(AppContext appContext)
        {
            return appContext.Context.Documents.Where(d => d.DocumentType == _clientDocumentType);
        }

        public Document GetDocument(int documentId, AppContext appContext)
        {
            return appContext.Context.Documents.Find(documentId);
        }
    }
}