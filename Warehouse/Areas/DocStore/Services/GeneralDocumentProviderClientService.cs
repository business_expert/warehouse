﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Warehouse.Areas.DocStore.Models;
using Warehouse.Libraries;
using AppContext = Warehouse.Libraries.AppContext;

namespace Warehouse.Areas.DocStore.Services
{
    public class GeneralDocumentProviderClientService : IDocumentProviderService
    {
        private readonly string _generalDocumentType = DocumentTypes.General.ToString();

        public IQueryable<Document> UserDocuments(AppContext appContext)
        {
            return CurrentUserDocuments(appContext);
        }

        public Document GetDocument(int documentId, AppContext appContext)
        {
            var documents = CurrentUserDocuments(appContext);
            return documents.First(d => d.Id == documentId);
        }

        private IQueryable<Document> CurrentUserDocuments(AppContext appContext)
        {
            var context = appContext.Context;
            var user = appContext.CurrentUser;

            var contractIds = user.Client.Contracts.Select(c => c.Id).ToList(); //TODO: check for contract expiration
            var categories = context.ContractCategoryRelations
                .Where(r => contractIds.Contains(r.ContractId))
                .Include(r => r.Category.Documents).Select(r => r.Category.Id)
                .ToList();
            return context.Documents.Where(
                d => d.Published && d.DocumentType == _generalDocumentType && 
                     (d.CategoryId == null || categories.Contains((int)d.CategoryId))
                     );
        }
    }
}