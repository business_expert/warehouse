﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Warehouse.Areas.DocStore.Models;
using Warehouse.Libraries;
using Warehouse.Models;
using AppContext = Warehouse.Libraries.AppContext;

namespace Warehouse.Areas.DocStore.Services
{
    public class GeneralDocumentProviderAdminService : IDocumentProviderService
    {
        private readonly string _generalDocumentType = DocumentTypes.General.ToString();

        public IQueryable<Document> UserDocuments(AppContext appContext)
        {
            return appContext.Context.Documents.Where(d => d.DocumentType == _generalDocumentType);
        }

        public Document GetDocument(int documentId, AppContext appContext)
        {
            return appContext.Context.Documents.Find(documentId);
        }
    }
}