﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.Practices.Unity;
using Warehouse.App_Start;
using Warehouse.Areas.DocStore.Models;
using Warehouse.Models;
using AppContext = Warehouse.Libraries.AppContext;

namespace Warehouse.Areas.DocStore.Services
{
    public class PublishStatus
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class DocumentService : IDocumentService
    {
        public DocumentService()
        {
            _unityContainer = UnityConfig.GetConfiguredContainer();
        }

        private readonly IUnityContainer _unityContainer;

        public List<Document> Documents(DocumentTypes documentType, Dictionary<string, string> searchCriteria)
        {
            var appContext = new AppContext();
            var documentProvider = DocumentProvider(documentType, appContext);
            var documents = documentProvider.UserDocuments(appContext);
            var filterService = new DocumentFilterService();
            var filteredDocuments = filterService.FilterDocument(documents, searchCriteria);
            if (DocumentTypes.Client == documentType)
            {
                filteredDocuments = filterService.FilterClientDocumentOwnership(filteredDocuments, searchCriteria, appContext);
            }
            return filteredDocuments.ToList();
        }

        public List<string> DocumentFileFormats(DocumentTypes documentType)
        {
            var appContext = new AppContext();
            var documentProvider = DocumentProvider(documentType, appContext);
            var filteredDocuments = documentProvider.UserDocuments(appContext);
            var paths = filteredDocuments.Select(document => document.Attachment.Path).ToList();
            return paths.Select(System.IO.Path.GetExtension).Distinct().ToList();
        }

        public Document CreateDocument(DocumentTypes documentType, Document document, ClientDocumentOwnership ownership)
        {
            var appContext = new AppContext();

            var attachment = appContext.Context.Attachments.Find(document.AttachmentId);
            document.Attachment = attachment;
            document.DocumentType = documentType.ToString();
            appContext.Context.Documents.Add(document);

            if (documentType == DocumentTypes.Client)
            {
                //TODO: move into IClientDocumentOwnershipService
                ownership.Document = document;
                appContext.Context.ClientDocumentOwnerships.Add(ownership);
            }

            appContext.Context.SaveChanges();
            return document;
        }

        public Document GetDocument(DocumentTypes documentType, int documentId)
        {
            var appContext = new AppContext();
            var documentProvider = DocumentProvider(documentType, appContext);
            var foundDocument = documentProvider.GetDocument(documentId, appContext);
            return foundDocument;
        }

        public Document UpdateDocument(DocumentTypes documentType, Document document, Dictionary<string, string> updatedFields, ClientDocumentOwnership ownership)
        {
            if (updatedFields.ContainsKey("CategoryId") && !updatedFields.ContainsKey("SubcategoryId"))
            {
                document.SubcategoryId = null;
                updatedFields["SubcategoryId"] = null;
            }

            var appContext = new AppContext();
            var documentProvider = DocumentProvider(documentType, appContext);
            var currentDocument = documentProvider.GetDocument(document.Id, appContext);
            foreach (var p in document.GetType().GetProperties().Where(p => updatedFields.ContainsKey(p.Name)))
            {
                p.SetValue(currentDocument, p.GetValue(document));
            }

            if (documentType == DocumentTypes.Client && ownership != null)
            {
                var ownershipService = _unityContainer.Resolve<IClientDocumentOwnershipService>();
                ownership.DocumentId = currentDocument.Id;
                ownershipService.UpdateOwnership(ownership);
            }

            appContext.Context.SaveChanges();
            return document;
        }

        public bool Destroy(DocumentTypes documentType, int documentId, out Document destroyedDocument)
        {
            var appContext = new AppContext();
            var documentProvider = DocumentProvider(documentType, appContext);
            destroyedDocument = documentProvider.GetDocument(documentId, appContext);
            appContext.Context.Documents.Remove(destroyedDocument);
            var result = appContext.Context.SaveChanges();

            return 1 == result;
        }

        public List<PublishStatus> DocumentPublishStatuses()
        {
            var list = new List<PublishStatus>
            {
                new PublishStatus {Name = "Published", Value = bool.TrueString},
                new PublishStatus {Name = "Unpublished", Value = bool.FalseString}
            };
            return list;
        }

        private IDocumentProviderService DocumentProvider(DocumentTypes documentType, AppContext appContext)
        {
            var namePrefix = appContext.IsCurrentUserAdmin ? "Admin" : "Client";
            var generalDocumentProviderName = $"{documentType}DocumentProvider{namePrefix}Service";
            var documentProvider = _unityContainer.Resolve<IDocumentProviderService>(generalDocumentProviderName);
            return documentProvider;
        }
    }
}