﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Warehouse.Areas.DocStore.Models;
using Warehouse.Libraries;

namespace Warehouse.Areas.DocStore.Services
{
    public class ClientCategoryProviderClientService : ICategoryProviderService
    {
        private readonly string _clientDocumentType = DocumentTypes.Client.ToString();

        public IQueryable<Category> Categories(Libraries.AppContext appContext)
        {
            //TODO: only the categories for the documents the client has
            return appContext.Context.Categories.Where(c => c.DocumentType == _clientDocumentType);
        }
    }
}