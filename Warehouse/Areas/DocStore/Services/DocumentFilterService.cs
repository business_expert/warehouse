﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Warehouse.Areas.DocStore.Models;
using Warehouse.Libraries;
using AppContext = Warehouse.Libraries.AppContext;

namespace Warehouse.Areas.DocStore.Services
{
    public class DocumentFilterService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="documents"></param>
        /// <param name="searchCriteria">fileinfo, filetype, category, subcategory, published</param>
        /// <returns></returns>
        public IQueryable<Document> FilterDocument(IQueryable<Document> documents, Dictionary<string, string> searchCriteria)
        {
            var filteredDocuments = documents;
            string fileinfo;
            if (searchCriteria.TryGetValue("fileinfo", out fileinfo))
            {
                var words = fileinfo.Split(' ').Where(word => !string.IsNullOrWhiteSpace(word));
                filteredDocuments = filteredDocuments.Where(d => words.All(word => (d.Title + " " + d.Notes).Contains(word)));
            }
            string filetype;
            if (searchCriteria.TryGetValue("filetype", out filetype))
            {
                filteredDocuments = filteredDocuments.Where(d => d.Attachment.Path.EndsWith(filetype));
            }
            string published;
            if (searchCriteria.TryGetValue("published", out published))
            {
                bool parsed;
                if (!bool.TryParse(published, out parsed))
                {
                    parsed = false;
                }
                filteredDocuments = filteredDocuments.Where(d => d.Published == parsed);
            }
            string category;
            if (searchCriteria.TryGetValue("category", out category))
            {
                var categoryId = Convert.ToInt32(category);
                filteredDocuments = filteredDocuments.Where(d => d.CategoryId == categoryId);
            }
            string subcategory;
            if (searchCriteria.TryGetValue("subcategory", out subcategory))
            {
                var subcategoryId = Convert.ToInt32(subcategory);
                filteredDocuments = filteredDocuments.Where(d => d.SubcategoryId == subcategoryId);
            }

            return filteredDocuments;
        }

        public IQueryable<Document> FilterClientDocumentOwnership(IQueryable<Document> documents, Dictionary<string, string> searchCriteria, AppContext appContext)
        {
            string client;
            var foundClient = searchCriteria.TryGetValue("client", out client);
            string branch;
            var foundBranch = searchCriteria.TryGetValue("branch", out branch);

            var filteredDocuments = documents;

            if (foundClient && foundBranch)
            {
                var clientId = Convert.ToInt32(client);
                filteredDocuments = from document in documents
                                    join ownership in appContext.Context.ClientDocumentOwnerships on document.Id equals ownership.DocumentId
                                    where ownership.ClientId == clientId && ownership.OwnerType == "Branch" && ownership.OwnerId == branch
                                    select document;
            }
            else if (foundClient)
            {
                var clientId = Convert.ToInt32(client);
                filteredDocuments = from document in documents
                                    join ownership in appContext.Context.ClientDocumentOwnerships on document.Id equals ownership.DocumentId
                                    where ownership.ClientId == clientId && (
                                        (ownership.OwnerType == "Client" && ownership.OwnerId == client) || (ownership.OwnerType == "Branch")
                                    )
                                    select document;
            }
            
            return filteredDocuments;
        }
    }
}