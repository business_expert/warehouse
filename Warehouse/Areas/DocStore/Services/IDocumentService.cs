﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Areas.DocStore.Models;

namespace Warehouse.Areas.DocStore.Services
{
    public interface IDocumentService
    {
        List<Document> Documents(DocumentTypes documentType, Dictionary<string, string> searchCriteria);
        List<string> DocumentFileFormats(DocumentTypes documentType);
        List<PublishStatus> DocumentPublishStatuses();
        Document CreateDocument(DocumentTypes documentType, Document document, ClientDocumentOwnership ownership);
        Document GetDocument(DocumentTypes documentType, int documentId);
        Document UpdateDocument(DocumentTypes documentType, Document document, Dictionary<string, string> updatedFields, ClientDocumentOwnership ownership);
        bool Destroy(DocumentTypes documentType, int documentId, out Document destroyedDocument);
    }
}
