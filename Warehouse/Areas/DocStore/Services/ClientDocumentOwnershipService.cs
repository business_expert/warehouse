﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Warehouse.Areas.DocStore.Models;
using AppContext = Warehouse.Libraries.AppContext;

namespace Warehouse.Areas.DocStore.Services
{
    public interface IClientDocumentOwnershipService
    {
        ClientDocumentOwnership OwnershipForDocument(int documentId);
        ClientDocumentOwnership UpdateOwnership(ClientDocumentOwnership ownership);
    }

    public class ClientDocumentOwnershipService : IClientDocumentOwnershipService
    {
        public ClientDocumentOwnership OwnershipForDocument(int documentId)
        {
            return GetOwnership(documentId, new AppContext());
        }

        public ClientDocumentOwnership UpdateOwnership(ClientDocumentOwnership ownership)
        {
            var appContext = new AppContext();
            var currentOwnership = GetOwnership(ownership.DocumentId, appContext);
            if (currentOwnership.ClientId != ownership.ClientId)
            {
                currentOwnership.ClientId = ownership.ClientId;
            }
            if (currentOwnership.OwnerType != ownership.OwnerType)
            {
                currentOwnership.OwnerType = ownership.OwnerType;
            }
            if (currentOwnership.OwnerId != ownership.OwnerId)
            {
                currentOwnership.OwnerId = ownership.OwnerId;
            }
            appContext.Context.SaveChanges();
            return currentOwnership;
        }

        private ClientDocumentOwnership GetOwnership(int documentId, AppContext appContext)
        {
            return appContext.Context.ClientDocumentOwnerships.First(o => o.DocumentId == documentId);
        }
    }
}