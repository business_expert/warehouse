﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Areas.DocStore.Models;
using AppContext = Warehouse.Libraries.AppContext;

namespace Warehouse.Areas.DocStore.Services
{
    interface ICategoryProviderService
    {
        IQueryable<Category> Categories(AppContext appContext);
    }
}
