﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Warehouse.Areas.DocStore.Models;
using AppContext = Warehouse.Libraries.AppContext;

namespace Warehouse.Areas.DocStore.Services
{
    public class GeneralCategoryProviderAdminService : ICategoryProviderService
    {
        private readonly string _generalDocumentType = DocumentTypes.General.ToString();

        public IQueryable<Category> Categories(AppContext appContext)
        {
            return appContext.Context.Categories.Where(c => c.DocumentType == _generalDocumentType);
        }
    }
}