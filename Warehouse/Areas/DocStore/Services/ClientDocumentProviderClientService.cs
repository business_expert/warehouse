﻿using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web;
using Warehouse.Areas.DocStore.Models;
using Warehouse.Libraries;

namespace Warehouse.Areas.DocStore.Services
{
    public class ClientDocumentProviderClientService : IDocumentProviderService
    {
        public IQueryable<Document> UserDocuments(Libraries.AppContext appContext)
        {
            var context = appContext.Context;

            var client = appContext.CurrentUser.Client;
            var clientIdStr = client.Id.ToString();
            var clientDocuments = context.ClientDocumentOwnerships.Where(
                o => o.OwnerType == "Client" && o.OwnerId == clientIdStr && o.Document.Published
                ).Select(o => o.Document);

            var currentUserId = appContext.CurrentUser.Id;
            var branchDocuments = from ownership in context.ClientDocumentOwnerships
                                  join branch in context.ApplicationUserBranches on ownership.OwnerId equals
                                      SqlFunctions.StringConvert((double)branch.BranchId).Trim()
                                  where ownership.OwnerType == "Branch" && branch.ApplicationUserId == currentUserId && ownership.Document.Published
                                  select ownership.Document;
            return clientDocuments.Union(branchDocuments);
        }

        public Document GetDocument(int documentId, Libraries.AppContext appContext)
        {
            throw new NotImplementedException();
        }
    }
}