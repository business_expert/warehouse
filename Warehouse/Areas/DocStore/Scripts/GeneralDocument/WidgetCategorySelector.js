﻿function WidgetCategorySelector(target) {
    var self = this;
    this.target = $(target);
    var subcategoryElementId = this.target.data("subcategory-element-id");
    this.subcategory = $("[data-element-id='" + subcategoryElementId + "']");
    var allSubcategoriesElementId = this.target.data("all-subcategories-element-id");
    this.allSubcategories = $("[data-element-id='" + allSubcategoriesElementId + "'] option");

    this.selectedCategoryId = function() { return self.target.val() };
    this.OnCategoryChange = function (event) {
        self.subcategory.find("option:not([value=''])").remove();

        var selectedCategoryId = self.selectedCategoryId();
        var isCategorySelected = (selectedCategoryId != ""); //not !==
        if (isCategorySelected) {
            self.allSubcategories.each(function (index, option) {
                var theOption = $(option);
                if (theOption.data("parent-category") == selectedCategoryId) { //not ===
                    theOption.clone().appendTo(self.subcategory);
                }
            });
        }

        self.subcategory.prop("disabled", !isCategorySelected);
    };

    this.ApplyPreSelected = function () {
        if (self.selectedCategoryId() != "") {
            self.subcategory.val(self.subcategory.data("pre-selected"));
        }
    };

    this.OnCategoryChange();
    this.ApplyPreSelected();
    this.target.change(this.OnCategoryChange);
}

$(function () {
    $("[data-widget-type='WidgetCategorySelector']").each(function (index, element) {
        new WidgetCategorySelector(element);
    });
});