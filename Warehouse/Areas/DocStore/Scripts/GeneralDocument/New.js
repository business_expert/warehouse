﻿$.validator.setDefaults({
    ignore: ":hidden:not([require-validation])"
}); //show the validation errors for the hidden fields

$(function () {
    var fileUploadControl = $("#FileUpload");
    fileUploadControl.on("attachment:created", function (event, attachmentId) {
        var hiddenAttachmentId = $("#AttachmentId");
        hiddenAttachmentId.val(attachmentId);
        var errorMessage = hiddenAttachmentId.nextAll(".field-validation-error");
        errorMessage.empty();
    });

    fileUploadControl.on("attachment:deleted", function(event, deletedAttachmentId) {
        var hiddenAttachmentId = $("#AttachmentId");
        if (hiddenAttachmentId.val() == deletedAttachmentId) {
            hiddenAttachmentId.val(null);
        }

        var hiddenDeletedAttachmentIds = $("#DeletedAttachmentIds");
        var existingDeleted = hiddenDeletedAttachmentIds.val();
        if (!existingDeleted) {
            existingDeleted = [];
        } else {
            existingDeleted = JSON.parse(existingDeleted);
        }
        existingDeleted.push(deletedAttachmentId);
        hiddenDeletedAttachmentIds.val(JSON.stringify(existingDeleted));
    });
});