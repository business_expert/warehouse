﻿var defaultApp = getDefaultApp();

defaultApp.controller("ClientBranchSelectorController", ["$scope", "$attrs", "$element", "$http", function ($scope, $attrs, $element, $http) {
    var clientSelector = $element.find("select[data-control-role='ClientSelector']");
    //var branchSelector = $element.find("select[data-control-role='BranchSelector']");
    var initialSelectedClient = clientSelector.find("option:selected");
    var initialSelectedBranch = $attrs.initialSelectedBranch;
    var branchPath = $attrs.branchPath;

    $scope.SelectedClient = initialSelectedClient.val();
    $scope.Branches = null;
    $scope.SelectedBranch = null;
    $scope.UpdateBranches = function (selectedBranch) {
        clientSelector.find("option[value='']").prop("disabled", true);
        $scope.SelectedBranch = null;
        $http.get(branchPath + "?clientId=" + $scope.SelectedClient).
            success(function (response) {
                var branches = response;
                $scope.Branches = branches;
                for (var i = 0; i < branches.length; i++) {
                    if (branches[i].Id == selectedBranch) {
                        $scope.SelectedBranch = branches[i];
                        break;
                    }
                }
            });
    };

    if ($scope.SelectedClient) {
        $scope.UpdateBranches(initialSelectedBranch);
    }

    //option to auto postback or not
}]);