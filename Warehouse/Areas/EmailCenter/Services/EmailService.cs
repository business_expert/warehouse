﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace Warehouse.Areas.EmailCenter.Services
{
    public interface IEmailService
    {
        void SendEmail(string email, string subject, string body);
    }

    public class EmailService : IEmailService
    {
        public void SendEmail(string email, string subject, string body)
        {
            Debug.WriteLine($"email service to: {email}::{subject}::{body}");

            MailMessage objEmail = new MailMessage();
            objEmail.Priority = MailPriority.Normal;
            objEmail.IsBodyHtml = true;
            objEmail.From = new MailAddress("info@warehouse.com"); // TODO: move to web.config
            objEmail.To.Add(new MailAddress(email));
            objEmail.Subject = subject;
            objEmail.Body = body;

            SmtpClient smtp = new SmtpClient();
            smtp.Send(objEmail);
        }
    }
}