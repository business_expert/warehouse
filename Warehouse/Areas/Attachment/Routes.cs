﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestfulRouting;
using Warehouse.Areas.Attachment.Controllers;

namespace Warehouse.Areas.Attachment
{
    public class Routes : RouteSet
    {
        public override void Map(IMapper map)
        {
            map.Area<AttachmentController>("Attachment", "", area =>
            {
                area.Resources<AttachmentController>();
            });
        }
    }
}