﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Warehouse.Areas.Attachment.Services;
using Warehouse.Libraries.UsersManagement;

namespace Warehouse.Areas.Attachment.Controllers
{
    public class AttachmentController : Controller
    {
        private readonly IAttachmentService _attachmentService;

        public AttachmentController(IAttachmentService attachmentService)
        {
            _attachmentService = attachmentService;
        }

        [Authorize(Roles = ApplicationRole.Admin)]
        public ActionResult Create(Models.Attachment attachment)
        {
            string preSignedUrl;
            var newAttachment = _attachmentService.CreateAttachment(Request.Params["Filename"], attachment, out preSignedUrl);
            return Json(new { url = preSignedUrl, attachmentId = newAttachment.Id });
        }

        [Authorize(Roles = ApplicationRole.Admin + "," + ApplicationRole.Client)]
        public ActionResult Show(int id)
        {
            var url = _attachmentService.GetAttachmentDownloadUrl(id);
            return Redirect(url);
        }

        //public ActionResult Update(Models.Attachment attachment)
        //{
        //    bool success = false;
        //    if (attachment.Confirmed)
        //    {
        //        success = _attachmentService.ConfirmAttachment(attachment.Id);
        //    }

        //    if (success)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.OK);
        //    }
        //    else
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //}

        //public ActionResult Destroy(int id)
        //{
        //    var success = _attachmentService.DestroyAttachment(id);

        //    if (success)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.OK);
        //    }
        //    else
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //}
    }
}