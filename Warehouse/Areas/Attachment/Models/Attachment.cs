﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Warehouse.Models;

namespace Warehouse.Areas.Attachment.Models
{
    public class Attachment : EntityBase
    {
        [Required]
        [Range(1, 10485760)] //in bytes
        public int Filesize { get; set; }
        [Required]
        public string MimeType { get; set; }

        public string Path { get; set; }
        public bool Confirmed { get; set; }

        public string OriginalFilename => System.IO.Path.GetFileName(Path);
        public string FileExtensionName => System.IO.Path.GetExtension(Path);
    }
}