﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using Amazon.S3;
using Amazon.S3.Model;
using Warehouse.Models;
using AppContext = Warehouse.Libraries.AppContext;

namespace Warehouse.Areas.Attachment.Services
{
    public interface IAttachmentService
    {
        Models.Attachment CreateAttachment(string filename, Models.Attachment attachment, out string preSignedUrl);
        bool ConfirmAttachment(int attachmentId);
        bool DestroyAttachment(int attachmentId);
        string GetAttachmentDownloadUrl(int attachmentId);
    }

    public class AttachmentService : IAttachmentService
    {
        public Models.Attachment CreateAttachment(string filename, Models.Attachment attachment, out string preSignedUrl)
        {
            attachment.Confirmed = false;
            var storageFilename = StorageFilename(filename);
            attachment.Path = storageFilename;
            preSignedUrl = CreateUploadPreSignedUrl(storageFilename, attachment.MimeType);

            var context = DbContext();
            context.Attachments.Add(attachment);
            context.SaveChanges();

            return attachment;
        }

        public bool ConfirmAttachment(int attachmentId)
        {
            var context = DbContext();
            var attachment = context.Attachments.Find(attachmentId);
            int resultCode;
            if (!attachment.Confirmed)
            {
                MoveFileToConfirmed(attachment.Path);
                attachment.Confirmed = true;
                resultCode = context.SaveChanges();
            }
            else
            {
                resultCode = 1;
            }

            return 1 == resultCode;
        }

        public bool DestroyAttachment(int attachmentId)
        {
            var context = DbContext();
            var attachment = context.Attachments.Find(attachmentId);
            DeleteConfirmedFile(attachment.Path);
            context.Attachments.Remove(attachment);
            int resultCode = context.SaveChanges();
            return 1 == resultCode;
        }

        public string GetAttachmentDownloadUrl(int attachmentId)
        {
            var context = DbContext();
            var attachment = context.Attachments.Find(attachmentId);
            var url = CreateDownloadPreSignedUrl(attachment.Path, attachment.OriginalFilename);
            return url;
        }

        private string CreateUploadPreSignedUrl(string storageFilename, string mimeType)
        {
            var path = UploadedFilePath(storageFilename);
            double timeLimit = Convert.ToDouble(ConfigurationManager.AppSettings["FileUploadTimeLimitSeconds"]);
            GetPreSignedUrlRequest request = new GetPreSignedUrlRequest
            {
                BucketName = ConfigurationManager.AppSettings["S3BucketName"],
                Key = path,
                ContentType = mimeType,
                Verb = HttpVerb.PUT,
                Expires = DateTime.Now.AddSeconds(timeLimit)
            };

            return SendPreSignedUrlRequest(request);
        }

        private string CreateDownloadPreSignedUrl(string storageFilename, string filename)
        {
            var path = ConfirmedFilePath(storageFilename);
            double timeLimit = Convert.ToDouble(ConfigurationManager.AppSettings["FileDownloadTimeLimitSeconds"]);
            GetPreSignedUrlRequest request = new GetPreSignedUrlRequest
            {
                BucketName = ConfigurationManager.AppSettings["S3BucketName"],
                Key = path,
                Verb = HttpVerb.GET,
                Expires = DateTime.Now.AddSeconds(timeLimit),
                ResponseHeaderOverrides = { ContentDisposition = $"attachment; filename={filename}" }
            };

            return SendPreSignedUrlRequest(request);
        }

        private string SendPreSignedUrlRequest(GetPreSignedUrlRequest request)
        {
            string url;
            using (IAmazonS3 s3Client = new AmazonS3Client())
            {
                url = s3Client.GetPreSignedURL(request);
            }

            return url;
        }

        private void MoveFileToConfirmed(string storageFilename)
        {
            var bucket = ConfigurationManager.AppSettings["S3BucketName"];
            var uploadedPath = UploadedFilePath(storageFilename);
            CopyObjectRequest copyRequest = new CopyObjectRequest
            {
                SourceBucket = bucket,
                SourceKey = uploadedPath,
                DestinationBucket = bucket,
                DestinationKey = ConfirmedFilePath(storageFilename)
            };
            DeleteObjectRequest deleteRequest = new DeleteObjectRequest { BucketName = bucket, Key = uploadedPath };
            using (IAmazonS3 s3Client = new AmazonS3Client())
            {
                s3Client.CopyObject(copyRequest);
                s3Client.DeleteObject(deleteRequest);
            }
        }

        private void DeleteConfirmedFile(string storageFilename)
        {
            var confirmedPath = ConfirmedFilePath(storageFilename);
            var bucket = ConfigurationManager.AppSettings["S3BucketName"];
            DeleteObjectRequest deleteRequest = new DeleteObjectRequest { BucketName = bucket, Key = confirmedPath };
            using (IAmazonS3 s3Client = new AmazonS3Client())
            {
                s3Client.DeleteObject(deleteRequest);
            }
        }

        private string StorageFilename(string originalFilename)
        {
            return $"{Guid.NewGuid()}/{originalFilename}";
        }

        private string UploadedFilePath(string storageFilename)
        {
            var folder = ConfigurationManager.AppSettings["S3BucketFolder"];
            return $"{folder}/Uploaded/{storageFilename}";
        }

        private string ConfirmedFilePath(string storageFilename)
        {
            var folder = ConfigurationManager.AppSettings["S3BucketFolder"];
            return $"{folder}/Confirmed/{storageFilename}";
        }

        private ApplicationDbContext DbContext()
        {
            return new AppContext().Context;
        }
    }
}