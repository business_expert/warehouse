﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Warehouse.Models
{
    public class ClientStyle:EntityBase
    {
        public string Key { get; set; }
        public string Value { get; set; }

        public int ClientId { get; set; }
        public virtual Client Client { get; set; }
    }
}