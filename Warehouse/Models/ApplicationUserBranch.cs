﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Warehouse.Models
{
    public class ApplicationUserBranch : EntityBase
    {
        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        public int BranchId { get; set; }
        public virtual Branch Branch { get; set; }
    }
}