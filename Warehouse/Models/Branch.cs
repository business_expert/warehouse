﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Warehouse.Models
{
    public class Branch : EntityBase
    {
        [Required]
        [Index("BranchUnique", 2, IsUnique = true)]
        [StringLength(300)]
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        [DataType(DataType.PostalCode)]
        public string PostCode { get; set; }
        [DataType(DataType.PhoneNumber)]
        public string Telephone { get; set; }
        [DataType(DataType.PhoneNumber)]
        public string Fax { get; set; }

        [Required]
        [Index("BranchUnique", 1, IsUnique = true)]
        public int ClientId { get; set; }
        public virtual Client Client { get; set; }

        public virtual ICollection<ApplicationUserBranch> ApplicationUserBranches { get; set; }
    }
}