﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Warehouse.Models
{
    public class Client : EntityBase
    {
        [Required(AllowEmptyStrings = false)]
        [Index(IsUnique = true)]
        [StringLength(300)]
        public string Name { get; set; }
        public string Address { get; set; }
        public bool IsAdminClient { get; set; }

        public virtual ICollection<Branch> Branches { get; set; }
        public virtual ICollection<ApplicationUser> ApplicationUsers { get; set; }
        public virtual ICollection<Contract> Contracts { get; set; }
        public virtual ICollection<ClientStyle> ClientStyles { get; set; }
    }
}