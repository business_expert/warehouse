﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Warehouse.Models
{
    public partial class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Branch> Branches { get; set; }
        public DbSet<ApplicationUserBranch> ApplicationUserBranches { get; set; }
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<ClientStyle> ClientStyles { get; set; }

        //public System.Data.Entity.DbSet<Warehouse.Models.ApplicationUser> ApplicationUsers { get; set; }

        /*protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<ApplicationUser>()
            //    .HasRequired(u => u.Branches)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Branch>()
                .HasRequired(b => b.appli)
                .WithMany()
                .WillCascadeOnDelete(false);
        }*/

        public override int SaveChanges()
        {
            //Intercept saving changes on the context to add more processing
            var entries = this.ChangeTracker.Entries();
            var changes = entries.Where(e => e.State == EntityState.Added || e.State == EntityState.Modified);

            var now = DateTime.Now;
            
            foreach (var entry in changes)
            {
                var type = entry.Entity.GetType();
                PropertyInfo property;

                property = type.GetProperty("CreatedAt");
                if (property != null)
                {
                    if (entry.State == EntityState.Added) //only for added
                    {
                        property.SetValue(entry.Entity, now, null); 
                    }
                    else
                    {
                        entry.Property("CreatedAt").IsModified = false;
                    }
                }

                //no matter added or modified
                property = type.GetProperty("UpdatedAt");
                property?.SetValue(entry.Entity, now, null);
            }

            return base.SaveChanges();
        }
    }
}